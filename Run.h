#ifndef RUN_H
#define RUN_H

#include "TF1.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TTreeReaderArray.h"
#include "TChain.h"
#include "TString.h"
#include "TFile.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TStyle.h"
#include "TFormula.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TMath.h"
#include <tuple>

using namespace std;

TString Categories[1]={"Nominal"};

class Run{

 public:
  Run(TString a, TString b, vector<TString> c, double N);
  void Analyze();
  void Analyze_2Dversion();
  vector <TH1F*>  GetHistos(TString a);

  bool doPrintout{false};
  double bin = 0.1;

 private:
  TString Name, Title;
  double Norm;
  vector <TString> Files;

  TChain *ch1, *ch2;
  TTreeReader *R1, *R2;


//   TTreeReaderArray<int>  *SC_channelId;
  TTreeReaderValue<short> *SC_BCID;
  TTreeReaderValue<unsigned long long int> *SC_IEvent;

  TTreeReaderValue<Int_t>   *SC_energyVec_ET_ID, *SC_channelId, *SC_Nsamples;
//   TTreeReaderArray<Double_t>  ;

  TTreeReaderArray<int> *TT_layer, *TT_jepET, *TT_cpET;
  TTreeReaderArray<float> *TT_eta, *TT_phi, *TT_cellET; 
  TTreeReaderArray<unsigned long long int> *TT_IEvent;

//   Float_t        energyVec_ET_ID[34048][5];
//   Double_t SCET[100][50][64] = {0};
//   Double_t TTET[100][50][64] = {0};
  Double_t Pi = 3.1415926535;

  void FillSCHistos(TString A, double w);
  void FillTTHistos(TString A, double w);
  void DefineBranches();
  void DefineHistos(TString a);
  void DrawPulse(int i);
  void DrawTauvEta();
  void DrawGraph();
  void DrawEMB();
  void DrawSCNoise();
  void DrawSCNoiseCut();
  void DrawTTNoise();
  void DrawTTNoiseCut();
  void SCvTT();


  int64_t twoCompl18bit(int64_t inputNumber);
  int64_t doTimeCut(int64_t ET,int64_t ETxtau);
  Double_t doTransverse(int64_t ET, double eta);




  vector <TH2D *> H2D1, H2D2, H2D3;

  map <TString, vector <TH1F *>> Htest, Histo;
  map <int, vector <TH1F *>> cell;
  map <int ,map <int, vector <TH1F *>>> HEnergy;

  map<int, map <int, map<int, Double_t>>> SCET;
  map<int, map <int, map<int, Double_t>>> CPET;
  map<int, map <int, map<int, Double_t>>> CELLET;

};

Run::Run(TString a, TString b,  vector<TString> c, double N){
 Name=a;
 Title=b;
 Files=c;
 Norm=N;
 cout<<"Object for "<<Name<<" created"<<endl;
 }

void Run::DefineHistos(TString a){

    Histo[a].push_back(new TH1F(a+"LARDIGITS_ET_ID"+Name, Title+"; Supercell ET (GeV) ; Entries ", 800, -100, 100));
    Histo[a].push_back(new TH1F(a+"LARDIGITS_ET_ID_log"+Name, Title+"; Supercell ET (GeV) ; Entries ", 1600, 0, 500));
    Histo[a].push_back(new TH1F(a+"LARDIGITS_ET_IDSUM_log"+Name, Title+"; ET (GeV) ; Entries ", 800, -100, 1000));
    Histo[a].push_back(new TH1F(a+"LARDIGITS_ET_ID2_log"+Name, Title+"; ET (GeV) ; Entries ", 800, -100, 1000));
    Histo[a].push_back(new TH1F(a+"LARDIGITS_ET_ID3_log"+Name, Title+"; ET (GeV) ; Entries ", 800, -100, 1000));
    Histo[a].push_back(new TH1F(a+"LARDIGITS_ET_ID4_log"+Name, Title+"; Sum of supercell ET (GeV) ; Entries ", 800, -100, 1000));

    Histo[a].push_back(new TH1F(a+"cellET_log"+Name, Title+"; cellET (GeV) ; Entries ", 500, 0, 500));
    Histo[a].push_back(new TH1F(a+"cpET_log"+Name, Title+"; cpET (GeV) ; Entries ", 1000, 0, 500));
    Histo[a].push_back(new TH1F(a+"jepET_log"+Name, Title+"; jepET (GeV) ; Entries ", 500, 0, 500));

    double k = bin;

    H2D1.push_back(new TH2D("layer0"," layer with detector=0 ; eta ; layer ; Events",  100 , -5 , 5 , 7 , -1 , 6 ));
    H2D1.push_back(new TH2D("layer1"," layer with detector=1 ; eta ; layer ; Events",  100 , -5 , 5 , 7 , -1 , 6 ));
    H2D1.push_back(new TH2D("layer2"," layer with detector=2 ; eta ; layer ; Events",  100 , -5 , 5 , 7 , -1 , 6 ));
    H2D1.push_back(new TH2D("layer3"," layer with detector=3 ; eta ; layer ; Events",  100 , -5 , 5 , 7 , -1 , 6 ));
    // H2D1.push_back(new TH2D("layer4"," layer with detector=4 ; eta ; layer ; Events",  100 , -5 , 5 , 7 , -1 , 6 ));

    H2D2.push_back(new TH2D("layer0SCET"," layer 0 ; eta ; phi ; ET(GeV)",  100 , -5 , 5 , 64 , -3.14 , 3.14 ));
    H2D2.push_back(new TH2D("layer1SCET"," layer 1 ; eta ; phi ; ET(GeV)",  400 , -5 , 5 , 64 , -3.14 , 3.14 ));
    H2D2.push_back(new TH2D("layer2SCET"," layer 2 ; eta ; phi ; ET(GeV)",  400 , -5 , 5 , 64 , -3.14 , 3.14 ));
    H2D2.push_back(new TH2D("layer3SCET"," layer 3 ; eta ; phi ; ET(GeV)",  100 , -5 , 5 , 64 , -3.14 , 3.14 ));
    H2D2.push_back(new TH2D("layersumSCET"," SC ; eta ; phi ; ET(GeV)",  100 , -5 , 5, 64 , -3.14 , 3.14 ));

    H2D2.push_back(new TH2D("EMlayerCPET"," layer EM ; eta ; phi ; cpET(GeV)",  100 , -5 , 5 , 64 , -3.14 , 3.14 ));
    H2D2.push_back(new TH2D("HadlayerCPET"," layer HAD ; eta ; phi ; cpET(GeV)",  100 , -5 , 5 , 64 , -3.14 , 3.14 ));
    H2D2.push_back(new TH2D("SumlayerCPET"," cp ; eta ; phi ; cpET(GeV)",  100 , -5 , 5, 64 ,-3.14 , 3.14 ));
    
    H2D2.push_back(new TH2D("EMlayerCELLET"," layer EM ; eta ; phi ; cellET(GeV)",  100 , -5 , 5 , 64 , -3.14 , 3.14 ));
    H2D2.push_back(new TH2D("HadlayerCELLET"," layer HAD ; eta ; phi ; cellET(GeV)",  100 , -5 , 5 , 64 , -3.14 , 3.14));
    H2D2.push_back(new TH2D("SumlayerCELLET"," Cell ; eta ; phi ; cellET(GeV)",  100 , -5 , 5, 64 , -3.14 , 3.14 ));

    H2D3.push_back(new TH2D("SCvTT"," SCvTT ; cpET (GeV); Sum of SC ET (GeV) ; ET(GeV)",  300, 0 , 300, 300 , 0 , 300 ));
    H2D3.push_back(new TH2D("SCvcell"," SCvCELL ; Sum of cell ET (GeV) ; Sum of SC ET (GeV) ;  ET(GeV)",  300 , 0 , 300, 300 , 0 , 300 ));
    H2D3.push_back(new TH2D("CPvcell"," SCvCELL ; Sum of cell ET (GeV) ; cpET(GeV);  ET(GeV)",  300 , 0 , 300, 300 , 0 , 300 ));


    for(int j=0; j<10.0/k; j++){

        TString a = std::to_string(j+1);
        TString b = std::to_string(-5.0 + j*k);
        TString c = std::to_string(-5.0 + k + j*k);    
        for(int l=0; l<4; l++){   
            TString d = std::to_string(l);
            // HEnergy[0][l].push_back(new TH1F("d0l"+d+"_"+a,"EMB Energy distribution, layer="+d+"("+b+" < eta < "+c+"); ET(GeV) ; Entries", 16000, -100, 100));
            // HEnergy[1][l].push_back(new TH1F("d1l"+d+"_"+a,"EMEC Energy distribution, layer="+d+"("+b+" < eta < "+c+"); ET(GeV) ; Entries", 16000, -100, 100));
            // HEnergy[2][l].push_back(new TH1F("d2l"+d+"_"+a,"HEC Energy distribution, layer="+d+"("+b+" < eta < "+c+"); ET(GeV) ; Entries", 16000, -100, 100));
            // HEnergy[3][l].push_back(new TH1F("d3l"+d+"_"+a,"FCAL Energy distribution, layer="+d+"("+b+" < eta < "+c+"); ET(GeV) ; Entries", 16000, -100, 100));

            HEnergy[0][l].push_back(new TH1F("d0l"+d+"_"+a,"EMB Energy distribution, layer="+d+"("+b+" < eta < "+c+"); ET(GeV) ; Entries", 1600, -10, 10));
            HEnergy[1][l].push_back(new TH1F("d1l"+d+"_"+a,"EMEC Energy distribution, layer="+d+"("+b+" < eta < "+c+"); ET(GeV) ; Entries", 1600, -10, 10));
            HEnergy[2][l].push_back(new TH1F("d2l"+d+"_"+a,"HEC Energy distribution, layer="+d+"("+b+" < eta < "+c+"); ET(GeV) ; Entries", 1600, -10, 10));
            HEnergy[3][l].push_back(new TH1F("d3l"+d+"_"+a,"FCAL Energy distribution, layer="+d+"("+b+" < eta < "+c+"); ET(GeV) ; Entries", 1600, -10, 10));
            // HEnergy[4][l].push_back(new TH1F("d4l"+d+"_"+a,"FCAL Energy distribution, layer="+d+"("+b+" < eta < "+c+"); ET(GeV) ; Entries", 1600, -10, 10));
        }
    }

    for(int j=0; j<10.0/k; j++){

        TString a = std::to_string(j+1);
        TString b = std::to_string(-5.0 + j*k);
        TString c = std::to_string(-5.0 + k + j*k);   
        cell[0].push_back(new TH1F("ttl0cp_"+a,"TTl0 Energy distribution("+b+" < eta < "+c+"); cpET(GeV) ; Events", 1000, -5, 5));
        cell[1].push_back(new TH1F("ttl1cp_"+a,"TTl1 Energy distribution("+b+" < eta < "+c+"); cpET(GeV) ; Events", 1000, -5, 5)); 
    }
    



 //for (int i=0; i<Histo[a].size(); i++) Histo[a][i]->Sumw2();
}



void Run::FillSCHistos(TString A, double w){

    Double_t eta, phi;
    int detector, layer;

    eta = ChannelLocation[**SC_channelId][0];
    phi = ChannelLocation[**SC_channelId][1];
    detector = ChannelLocation[**SC_channelId][2];
    layer = ChannelLocation[**SC_channelId][3];


    // cout<<"------1-----"<<endl;

    int M = (eta+5.0)/bin;

    switch(detector){
        case 0: 
            H2D1[0]->Fill(eta, layer, 1);
            break;        
        case 1: 
            H2D1[1]->Fill(eta, layer, 1);
            break;
        case 2: 
            H2D1[2]->Fill(eta, layer, 1);
            break;
        case 3: 
            H2D1[3]->Fill(eta, layer, 1);
            break;
        
    }


//    cout<<"-------------"<<"i="<<i<<", ChannelID: "<<ChannelId<<", detector : "<<SC_detector->At(i)<<", layer : "<<SC_layer->At(i)<<"---------------------"<<endl;


    Histo[A][0]->Fill(**SC_energyVec_ET_ID*12.5/1000);
    Histo[A][1]->Fill(**SC_energyVec_ET_ID*12.5/1000);


    if(**SC_energyVec_ET_ID*12.5/1000>=5){
        H2D2[layer]->Fill(eta, phi, **SC_energyVec_ET_ID*12.5/1000);
        // H2D2[4]->Fill(eta, phi, **SC_energyVec_ET_ID*12.5/1000);
        }

    if( M >= 0 && M < 10.0/bin){
        if(**SC_energyVec_ET_ID>=-131,072){ //ET_ID[1]!=0||ET_ID[1]==ET[1]
            // HEnergy[detector][layer][M]->Fill(TET_ID[1]*12.5/1000,1);
            HEnergy[detector][layer][M]->Fill(**SC_energyVec_ET_ID*12.5/1000,1);
        }
    }
    // cout<<"--------------"<<endl;



}

void Run::FillTTHistos(TString A, double w){

    for(int i=0; i<7168;i++){

        int M = (TT_eta->At(i)+5.0)/bin;
        
        Int_t ttlayer = 5 + TT_layer->At(i);
        Double_t ttphi = TT_phi->At(i);
        if(ttphi>Pi) ttphi = ttphi - 2*Pi;
        if(TT_cpET->At(i)*0.5>5){
            H2D2[ttlayer]->Fill(TT_eta->At(i), ttphi, TT_cpET->At(i)*0.5);
            H2D2[7]->Fill(TT_eta->At(i), ttphi, TT_cpET->At(i)*0.5);
        }
        
        if(TT_cellET->At(i)>5){
            H2D2[ttlayer+3]->Fill(TT_eta->At(i), ttphi, TT_cellET->At(i));
            H2D2[10]->Fill(TT_eta->At(i), ttphi, TT_cellET->At(i));
        }
        // cout<<"i : "<<i<<endl;
        Histo[A][6]->Fill(TT_cellET->At(i));
        // cout<<"------cell--------"<<endl;
        Histo[A][7]->Fill(TT_cpET->At(i)*0.5);
        // cout<<"------cp--------"<<endl;
        Histo[A][8]->Fill(TT_jepET->At(i));
        // cout<<"-------jep-------"<<endl;

        if( M >= 0 && M < 10.0/bin){
            if(true){ //  >-10?
                cell[TT_layer->At(i)][M]->Fill(TT_cpET->At(i)*0.5,1);
            }
        }




    }

}

void Run::DrawSCNoise(){

    TCanvas *c1 = new TCanvas();
    c1->SetGrid();
    // map <int, vector<TGraph*>> gr;
    map <int, vector<TGraphErrors*>> gr;
    TMultiGraph *mg = new TMultiGraph();

    int n = 10.0/bin;
    double x[n], y[n], ex[n],ey[n];

    for(int d=0; d<4; d++){
        for(int l=0; l<4; l++){
            for(int i=0; i<n; i++){
                x[i] = 0;
                y[i] = 0;
                ex[i] = 0;
                ey[i] = 0; //reset

                TF1 *func1 = HEnergy[d][l][i]->GetFunction("gaus");

                if(func1) {
                    x[i] = i*bin-5.0+bin/2;
                    ex[i] = 0; //bin/2;
                    y[i] = HEnergy[d][l][i]->GetStdDev();
                    ey[i] = 0;

                    // myfile1 <<",RMS, " <<d<<","<<l<<", "<<x[i]<<", "<<y[i]<<", "<<endl;
                }
            }
        gr[d].push_back(new TGraphErrors(n,x,y,ex,ey));
        for(int p=99; p>=0; p--){
            if(x[p]==0 && y[p]==0){
                 gr[d][l]->RemovePoint(p);  //remove missing data point
            }
        }


        }

    }

    mg->Add(gr[0][0]);
    mg->Add(gr[0][1]);
    mg->Add(gr[0][2]);
    mg->Add(gr[0][3]);

    mg->Add(gr[1][0]);
    mg->Add(gr[1][1]);
    mg->Add(gr[1][2]);
    mg->Add(gr[1][3]);

    mg->Add(gr[2][0]);

    mg->Add(gr[3][1]);
    mg->Add(gr[3][2]);
    mg->Add(gr[3][3]);
    

    gr[0][0]->SetTitle("EMB pre-sampler");
    gr[0][0]->SetMarkerColor(4);
    gr[0][0]->SetMarkerStyle(21);
    gr[0][1]->SetTitle("EMB front");
    gr[0][1]->SetMarkerColor(3);
    gr[0][1]->SetMarkerStyle(22);
    gr[0][2]->SetTitle("EMB middle");
    gr[0][2]->SetMarkerColor(2);
    gr[0][2]->SetMarkerStyle(20);
    gr[0][3]->SetTitle("EMB back");
    gr[0][3]->SetMarkerColor(1);
    gr[0][3]->SetMarkerStyle(47);

    gr[1][0]->SetTitle("EMEC pre-sampler");
    gr[1][0]->SetMarkerColor(4);
    gr[1][0]->SetMarkerStyle(22);
    gr[1][1]->SetTitle("EMEC layer 1");
    gr[1][1]->SetMarkerColor(3);
    gr[1][1]->SetMarkerStyle(3);
    gr[1][2]->SetTitle("EMEC layer 2");
    gr[1][2]->SetMarkerColor(2);
    gr[1][2]->SetMarkerStyle(21);
    gr[1][3]->SetTitle("EMEC layer 3");
    gr[1][3]->SetMarkerColor(1);
    gr[1][3]->SetMarkerStyle(22);

    gr[2][0]->SetTitle("HEC layer 0");
    gr[2][0]->SetMarkerColor(4);
    gr[2][0]->SetMarkerStyle(2);

    gr[3][1]->SetTitle("FCAL layer 1");
    gr[3][1]->SetMarkerColor(3);
    gr[3][1]->SetMarkerStyle(8);
    gr[3][2]->SetTitle("FCAL layer 2");
    gr[3][2]->SetMarkerColor(2);
    gr[3][2]->SetMarkerStyle(23);
    gr[3][3]->SetTitle("FCAL layer 3");
    gr[3][3]->SetMarkerColor(1);
    gr[3][3]->SetMarkerStyle(34);

    TPaveText* label = new TPaveText(0.11, 0.79, 0.43, 0.94, "ndc");
	label->SetBorderSize(0);
	label->SetFillStyle(0);
	label->SetTextAlign(13);

	TText* text = label->AddText("#it{ATLAS} #bf{Work in Progress}");
	text->SetTextSize(0.06);
	// text = label->AddText("#bf{Run 00440199}");
    // text = label->AddText("#bf{Run 00440407}");
	// text->SetTextSize(0.045);
    // text = label->AddText("#bf{Stable beam}");
    // text = label->AddText("#it{Physics_CosmicCalo}");
    // text = label->AddText("#it{Zerobias}");
	// text->SetTextSize(0.045);

    c1->SetLogy();
    mg->SetTitle("ET Noise after timing cut; eta ; RMS (GeV)");
    mg->Draw("AP");
    c1->BuildLegend(0.85,0.9,1.0,0.5);
    label->Draw();
    c1->SaveAs("Histos/ETNoise.png");

}

Double_t FindNoiseCut(TH1F *h){ 

    Int_t n = 0;
    Int_t N = h->GetNbinsX();
    Double_t entries = h->GetEntries();
    Int_t i = 0;

    for(i = N; i >= 0; i--){

        n += h->GetBinContent(i);
        if(n*1.0/entries>0.005) break;  // 1% / 0.1% / 0.05% / 2% / 5%  cut
    }

    return h->GetBinLowEdge(i);
}


void Run::DrawSCNoiseCut(){

    TCanvas *c1 = new TCanvas();
    c1->SetGrid();
    map <int, vector<TGraph*>> gr;
    TMultiGraph *mg = new TMultiGraph();

    int n = 10.0/bin;
    double x[n], y[n], ex[n],ey[n];

    for(int d=0; d<4; d++){
        for(int l=0; l<4; l++){
            for(int i=0; i<n; i++){
                x[i] = 0;
                y[i] = 0;
                ex[i] = 0;
                ey[i] = 0; //reset

                TF1 *func1 = HEnergy[d][l][i]->GetFunction("gaus");

                if(func1) {
                    x[i] = i*bin-5.0+bin/2;
                    ex[i] = 0; //bin/2;
                    y[i] = FindNoiseCut(HEnergy[d][l][i]);  // Noise cut 
                    ey[i] = 0;
                    // cout<<"------"<<endl;
                    myfile1 <<", " <<d<<","<<l<<", "<<x[i]<<", "<<y[i]<<", "<<endl;
                }
            }
        gr[d].push_back(new TGraphErrors(n,x,y,ex,ey));
        for(int p=99; p>=0; p--){
            if(x[p]==0 && y[p]==0){
                 gr[d][l]->RemovePoint(p);  //remove missing data point
            }
        }


        }

    }

    mg->Add(gr[0][0]);
    mg->Add(gr[0][1]);
    mg->Add(gr[0][2]);
    mg->Add(gr[0][3]);

    mg->Add(gr[1][0]);
    mg->Add(gr[1][1]);
    mg->Add(gr[1][2]);
    mg->Add(gr[1][3]);

    mg->Add(gr[2][0]);

    mg->Add(gr[3][1]);
    mg->Add(gr[3][2]);
    mg->Add(gr[3][3]);
    

    gr[0][0]->SetTitle("EMB pre-sampler");
    gr[0][0]->SetMarkerColor(4);
    gr[0][0]->SetMarkerStyle(21);
    gr[0][1]->SetTitle("EMB front");
    gr[0][1]->SetMarkerColor(3);
    gr[0][1]->SetMarkerStyle(22);
    gr[0][2]->SetTitle("EMB middle");
    gr[0][2]->SetMarkerColor(2);
    gr[0][2]->SetMarkerStyle(20);
    gr[0][3]->SetTitle("EMB back");
    gr[0][3]->SetMarkerColor(1);
    gr[0][3]->SetMarkerStyle(47);

    gr[1][0]->SetTitle("EMEC pre-sampler");
    gr[1][0]->SetMarkerColor(4);
    gr[1][0]->SetMarkerStyle(22);
    gr[1][1]->SetTitle("EMEC layer 1");
    gr[1][1]->SetMarkerColor(3);
    gr[1][1]->SetMarkerStyle(3);
    gr[1][2]->SetTitle("EMEC layer 2");
    gr[1][2]->SetMarkerColor(2);
    gr[1][2]->SetMarkerStyle(21);
    gr[1][3]->SetTitle("EMEC layer 3");
    gr[1][3]->SetMarkerColor(1);
    gr[1][3]->SetMarkerStyle(22);

    gr[2][0]->SetTitle("HEC layer 0");
    gr[2][0]->SetMarkerColor(4);
    gr[2][0]->SetMarkerStyle(2);

    gr[3][1]->SetTitle("FCAL layer 1");
    gr[3][1]->SetMarkerColor(3);
    gr[3][1]->SetMarkerStyle(8);
    gr[3][2]->SetTitle("FCAL layer 2");
    gr[3][2]->SetMarkerColor(2);
    gr[3][2]->SetMarkerStyle(23);
    gr[3][3]->SetTitle("FCAL layer 3");
    gr[3][3]->SetMarkerColor(1);
    gr[3][3]->SetMarkerStyle(34);

    //mg->SetTitle("ET Noise (Pilot Run405543); eta ; Noise (GeV)");
    TPaveText* label = new TPaveText(0.11, 0.79, 0.43, 0.94, "ndc");
	label->SetBorderSize(0);
	label->SetFillStyle(0);
	label->SetTextAlign(13);

	TText* text = label->AddText("#it{ATLAS} #bf{Work in Progress}");
	text->SetTextSize(0.06);
	// text = label->AddText("#bf{Run 00440407}");
    // text = label->AddText("#bf{Run 00440199}");
	// text->SetTextSize(0.045);
    // text = label->AddText("#bf{Stable beam}");
    // text = label->AddText("#it{Physics_CosmicCalo}");
    // text = label->AddText("#it{Zerobias}");
	// text->SetTextSize(0.045);


    c1->SetLogy();
    mg->SetTitle(" Noise cut; eta ; ET (GeV)");
    mg->Draw("AP");
    c1->BuildLegend(0.85,0.9,1.0,0.5);
    label->Draw();
    c1->SaveAs("Histos/ETNoiseCut.png");

}

void Run::DrawTTNoise(){

    TCanvas *c1 = new TCanvas();
    c1->SetGrid();
    vector<TGraphErrors*> gr;
    TMultiGraph *mg = new TMultiGraph();

    int n = 10.0/bin;
    double x[n], y[n], ex[n],ey[n];

    for(int l=0; l<2; l++){
            for(int i=0; i<n; i++){
                x[i] = 0;
                y[i] = 0;
                ex[i] = 0;
                ey[i] = 0; //reset

                TF1 *func1 = cell[l][i]->GetFunction("gaus");

                if(func1) {
                    x[i] = i*bin-5.0+bin/2;
                    ex[i] = 0; //bin/2;
                    y[i] = cell[l][i]->GetStdDev();
                    ey[i] = 0;
                }
            }
        gr.push_back(new TGraphErrors(n,x,y,ex,ey));
        for(int p=99; p>=0; p--){
            if(x[p]==0 && y[p]==0){
                 gr[l]->RemovePoint(p);  //remove missing data point
            }
        }

    }

    mg->Add(gr[0]);
    mg->Add(gr[1]);


    gr[0]->SetTitle("TT EM layer");
    gr[0]->SetMarkerColor(4);
    gr[0]->SetMarkerStyle(20);
    gr[1]->SetTitle("TT Hadronic layer");
    gr[1]->SetMarkerColor(2);
    gr[1]->SetMarkerStyle(22);

    TPaveText* label = new TPaveText(0.11, 0.79, 0.43, 0.94, "ndc");
	label->SetBorderSize(0);
	label->SetFillStyle(0);
	label->SetTextAlign(13);

	TText* text = label->AddText("#it{ATLAS} #bf{Work in Progress}");
	text->SetTextSize(0.06);
	// text = label->AddText("#bf{Run 00440407}");
	// text->SetTextSize(0.045);
    // text = label->AddText("#bf{Stable beam}");
    // text = label->AddText("#it{Physics_CosmicCalo}");
	// text->SetTextSize(0.045);


    c1->SetLogy();
    mg->SetTitle("TT; eta ; RMS (GeV)");
    mg->Draw("AP");
    c1->BuildLegend(0.8,0.9,1.0,0.8);
    label->Draw();
    c1->SaveAs("Histos/TTETRMS.png");

}


void Run::DrawTTNoiseCut(){

    TCanvas *c1 = new TCanvas();
    c1->SetGrid();
    vector<TGraphErrors*> gr;
    TMultiGraph *mg = new TMultiGraph();

    int n = 10.0/bin;
    double x[n], y[n], ex[n],ey[n];

        for(int l=0; l<2; l++){
            for(int i=0; i<n; i++){
                x[i] = 0;
                y[i] = 0;
                ex[i] = 0;
                ey[i] = 0; //reset

                TF1 *func1 = cell[l][i]->GetFunction("gaus");

                if(func1) {
                    x[i] = i*bin-5.0+bin/2;
                    ex[i] = 0; //bin/2;
                    y[i] = FindNoiseCut(cell[l][i]);  // Noise cut 
                    ey[i] = 0;
                }
            }
        gr.push_back(new TGraphErrors(n,x,y,ex,ey));
        for(int p=99; p>=0; p--){
            if(x[p]==0 && y[p]==0){
                 gr[l]->RemovePoint(p);  //remove missing data point
            }
        }
        }

    

    mg->Add(gr[0]);
    mg->Add(gr[1]);


    gr[0]->SetTitle("TT EM layer");
    gr[0]->SetMarkerColor(4);
    gr[0]->SetMarkerStyle(20);
    gr[1]->SetTitle("TT Hadronic layer");
    gr[1]->SetMarkerColor(2);
    gr[1]->SetMarkerStyle(22);

    TPaveText* label = new TPaveText(0.59, 0.18, 0.90, 0.33, "ndc");
    label->SetBorderSize(0);
    label->SetFillStyle(0);
    label->SetTextAlign(13);

	TText* text = label->AddText("#it{ATLAS} #bf{Work in Progress}");
	text->SetTextSize(0.06);
	// text = label->AddText("#bf{Run 00440407}");
	// text->SetTextSize(0.045);
    // text = label->AddText("#bf{Stable beam}");
    // text = label->AddText("#it{Physics_CosmicCalo}");
	// text->SetTextSize(0.045);

    // c1->SetLogy();
    mg->SetTitle(" 1% Noise cut; eta ; ET (GeV)");
    mg->Draw("AP");
    c1->BuildLegend(0.8,0.9,1.0,0.8);
    label->Draw();
    c1->SaveAs("Histos/TTET1pCut.png");
    // c1->SaveAs("Histos/TTET0p5pCut.png");
    // c1->SaveAs("Histos/TTET0p1pCut.png");
}





vector <TH1F*> Run::GetHistos(TString a){return Histo[a];}

void Run::DefineBranches(){

 ch1 = new TChain("SCDIGITS");
//  ch2 = new TChain("TRIGGERTOWERS");
 for (int i=0; i<Files.size(); i++) {
    ch1->Add(Files[i]);
    // ch2->Add(Files[i]);
 }

 R1 = new TTreeReader(ch1);
//  R2 = new TTreeReader(ch2);


 SC_channelId   = new TTreeReaderValue<int>              (*R1,"channelId");
 SC_IEvent   = new TTreeReaderValue<unsigned long long int>      (*R1,"IEvent");
//  SC_BCID   = new TTreeReaderValue<short>                 (*R1,"BCID");
//  SC_Nsamples = new TTreeReaderValue<int>                 (*R1,"Nsamples");
//  SC_samples = new TTreeReaderArray<short>                (*R1,"samples");
//  SC_samples_ADC_BAS = new TTreeReaderArray<short>                (*R1,"samples_ADC_BAS");
//  bcidVec_ET = new TTreeReaderArray<unsigned short>             (*R,"bcidVec_ET");
//  energyVec_ET_ID   = new TTreeReaderArray<int>                    (*R,"energyVec_ET_ID");
 SC_energyVec_ET_ID   = new TTreeReaderValue<int>                    (*R1,"energyVec_ET_ID");

//  TT_IEvent   = new TTreeReaderArray<unsigned long long int>      (*R2,"IEvent");
//  TT_eta   = new TTreeReaderArray<float>                  (*R2,"eta");
//  TT_phi   = new TTreeReaderArray<float>                  (*R2,"phi");
//  TT_layer   = new TTreeReaderArray<int>                    (*R2,"layer");
//  TT_jepET   = new TTreeReaderArray<int>                    (*R2,"jepET");
//  TT_cpET   = new TTreeReaderArray<int>                    (*R2,"cpET");
//  TT_cellET   = new TTreeReaderArray<float>                  (*R2,"cellET");

}

void Run::Analyze(){

 TFile * myFile = new TFile("testSC.root","recreate");
 DefineBranches();
 DefineHistos(Categories[0]);
 //DefinePulseHistos(Categories[0]);
 

 int I=0, J=0;


 Int_t nMaxEventsToProcess_SC = 10000000; // ~75000000
 Int_t nMaxEventsToProcess_TT = 2500;  // ~2500
 while (R1->Next()) { I++;

    // if (**SC_IEvent!=198311933) continue;
    if (I>nMaxEventsToProcess_SC) break;
    if (I%1000000==0) cout<<I<<endl;
    
    // std::cout <<I<<", SC_iEvent: "<<**SC_IEvent<< std::endl;
    FillSCHistos("Nominal", 1);

    // if(abs(ChannelLocation[**SC_channelId][0])<2.5){

    // Int_t eta1 = (ChannelLocation[**SC_channelId][0]+2.5)/0.1;
    // Double_t scphi = ChannelLocation[**SC_channelId][1];
    // if(scphi < 0) scphi = scphi +2*Pi;
    
    // Int_t phi1 = scphi*64/(2*Pi);
    // SCET[**SC_IEvent][eta1][phi1] += **SC_energyVec_ET_ID*12.5/1000;
    // }
    // if(**SC_energyVec_ET_ID*12.5/1000>=0){
    //     H2D2[4]->Fill(ChannelLocation[**SC_channelId][0], ChannelLocation[**SC_channelId][1], **SC_energyVec_ET_ID*12.5/1000);
    // }

 }


    // for(Int_t i=0; i<=H2D2[4]->GetNbinsX(); i++){
    //     for(Int_t j=0; j<=H2D2[4]->GetNbinsY(); j++){
    //         if(H2D2[4]->GetBinContent(i,j)<=5)  H2D2[4]->SetBinContent(i,j,0);

    //     }

    // }



//  cout<<"--------start fill TT----------"<<endl;
//  while (R2->Next()) { J++;

//     if (J>nMaxEventsToProcess_TT) break;
//     if (J%100==0) cout<<J<<endl;
    
//     // std::cout <<J<<", TT_iEvent: "<<TT_IEvent->At(0)<< std::endl;
//     FillTTHistos("Nominal", 1);

//     for(Int_t k = 0; k<7168; k++){

//             if(abs(TT_eta->At(k))<2.5){
//                 Double_t ttphi = TT_phi->At(k);
//                 // if(ttphi>Pi) ttphi = ttphi - 2*Pi;

//                 Int_t eta2 = (TT_eta->At(k)+2.5)/0.1;
//                 Int_t phi2 = (ttphi)*64/(2*Pi);
//                 CPET[TT_IEvent->At(k)][eta2][phi2] += TT_cpET->At(k)*0.5;
//                 CELLET[TT_IEvent->At(k)][eta2][phi2] += TT_cellET->At(k);
//             }   
//     }
//         for(Int_t m = 0; m<50; m++){
//             for(Int_t n = 0; n<64; n++){
//                 if(SCET[TT_IEvent->At(0)][m][n]>=0 || CELLET[TT_IEvent->At(0)][m][n]>=0|| CPET[TT_IEvent->At(0)][m][n]>=0){

                    

//                                             // cout<<"eta:"<<m<<", phi: "<<n<<", cell: "<<CELLET[TT_IEvent->At(0)][m][n]<<", sc: "<<SCET[TT_IEvent->At(0)][m][n]<<", cp: "<<CPET[TT_IEvent->At(0)][m][n]<<endl;
//                 if(TT_layer->At(0) == 0){
//                 H2D3[0]->Fill(CPET[TT_IEvent->At(0)][m][n],SCET[TT_IEvent->At(0)][m][n],1);   
//                 H2D3[1]->Fill(CELLET[TT_IEvent->At(0)][m][n],SCET[TT_IEvent->At(0)][m][n],1);
//                 H2D3[2]->Fill(CELLET[TT_IEvent->At(0)][m][n],CPET[TT_IEvent->At(0)][m][n],1);
//                 }

//                 // Histo["Nominal"][2]->Fill(SCET[TT_IEvent->At(0)][m][n]);
                    
//                 }

            // }
        // }



//  }


 cout<<"Events : "<< I << endl;


//----------------Drawing---------------

    for(int j=0; j<H2D1.size(); j++) {
        string Save3(H2D1[j]->GetName());
        TString SaveAs3="Histos/"+Save3+".png";
        TCanvas* C=new TCanvas();
         gStyle->SetOptStat(0);
        H2D1[j]->Draw("Y+");
        H2D1[j]->Draw("SCAT");
        C->SaveAs(SaveAs3);
    }

    // for(int j=0; j<H2D2.size(); j++) {
    //     string Save3(H2D2[j]->GetName());
    //     TString SaveAs3="Histos/"+Save3+".png";
    //     TCanvas* C=new TCanvas();
    //     gStyle->SetOptStat(0);
           
    //     TPaveText* label = new TPaveText(0.44, 0.78, 0.80, 0.90, "ndc");
    //     label->SetBorderSize(0);
    //     label->SetFillStyle(0);
    //     label->SetTextAlign(13);
    //     TText* text = label->AddText("#it{ATLAS} #bf{Work in Progress}");
    //     text->SetTextSize(0.05);
    //     // text = label->AddText("#bf{Run 00440407, IEvent = 198311933}");
	//     // text->SetTextSize(0.04);
    //     // text = label->AddText("#bf{IEvent =199806833}"); //198311933 //199806833
	//     // text->SetTextSize(0.04);
    //     text = label->AddText("#bf{E_{T}> 5 GeV}");
	//     text->SetTextSize(0.04);
        
    //     H2D2[j]->Draw("COLZ");
    //     label->Draw();
    //     // C->SetLogz();
    //     C->SaveAs(SaveAs3);
    // }

    // for(int j=0; j<H2D3.size(); j++) {
    //     string Save4(H2D3[j]->GetName());
    //     TString SaveAs4="Histos/"+Save4+".png";
    //     TCanvas* C=new TCanvas();
    //      gStyle->SetOptStat(0);
    //     TPaveText* label = new TPaveText(0.34, 0.15, 0.55, 0.33, "ndc");
    //     label->SetBorderSize(0);
    //     label->SetFillStyle(0);
    //     label->SetTextAlign(13);
    //     TText* text = label->AddText("#it{ATLAS} #bf{Work in Progress}");
    //     text->SetTextSize(0.06);
    //     text = label->AddText("#bf{Run 00440407}");
	//     text->SetTextSize(0.045);
    //     text = label->AddText("#bf{|#eta|<2.5}");
	//     text->SetTextSize(0.045);
    //     H2D3[j]->Draw("COLZ");
    //     label->Draw();
    //     C->SaveAs(SaveAs4);
    // }

   for(int d=0; d<4; d++){
       for(int l=0; l<4; l++){
           for(int i=0; i<100; i++){
            TCanvas* C=new TCanvas();
            string Save1(HEnergy[d][l][i]->GetName());
            TString SaveAs1="testHistos/"+Save1+".png";
            HEnergy[d][l][i]->Fit("gaus","Q");
            C->SetLogy();
            HEnergy[d][l][i]->Draw();
            C->SaveAs(SaveAs1);
           }
       }
   }
   
    // for(int l=0; l<2; l++){
    // for(int i=0; i<100; i++){
    //     TCanvas* C=new TCanvas();
    //     string Save1(cell[l][i]->GetName());
    //     TString SaveAs1="TTHistos/"+Save1+".png";
    //     cell[l][i]->Fit("gaus","Q");
    //     C->SetLogy();
    //     cell[l][i]->Draw();
    //     C->SaveAs(SaveAs1);
    // }
    // }

 DrawSCNoise();
 DrawSCNoiseCut();

//  DrawTTNoise();
//  DrawTTNoiseCut();
 

}




#endif
