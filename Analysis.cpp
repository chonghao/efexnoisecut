#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <string_view>

ofstream myfile1;

std::map<int, std::vector<int>> ChannelInfo;
std::map<int, std::vector<float>> ChannelLocation;

#include "Run.h"

using namespace std;

vector<Run*> R;

void DrawMultiHistos(vector<TH1*> h, TString LegTitle, bool Nor);

void ReadChannelInfoFromRoot(std::string File){

    std::unique_ptr<TFile> myFile( TFile::Open(File.c_str()) );
  
    auto my_tree = myFile->Get<TTree>("treeSCcounts");

    int channelId;
    float pedestal, sc_eta, sc_phi;
    short sc_det, sc_layer;
    int pedestal_int;

    bool doPrint=false;  

    auto ofc_a_ptr = new std::vector<float>();
    auto ofc_a_int_ptr = new std::vector<int>();
    auto ofc_b_ptr = new std::vector<float>();
    auto ofc_b_int_ptr = new std::vector<int>();

    my_tree->SetBranchAddress("channelId", &channelId);
    my_tree->SetBranchAddress("sc_ped", &pedestal);

    my_tree->SetBranchAddress("sc_ofca_cali", &ofc_a_ptr);
    my_tree->SetBranchAddress("sc_ofca_cali_int", &ofc_a_int_ptr);
    my_tree->SetBranchAddress("sc_ofcb_cali", &ofc_b_ptr);
    my_tree->SetBranchAddress("sc_ofcb_cali_int", &ofc_b_int_ptr);

    my_tree->SetBranchAddress("sc_eta", &sc_eta);
    my_tree->SetBranchAddress("sc_phi", &sc_phi);
    my_tree->SetBranchAddress("sc_det", &sc_det);
    my_tree->SetBranchAddress("sc_layer", &sc_layer);

    std::cout << "Loading filter coefficients and supercell information from file " << File << std::endl;

    int nMaxEntries = 1000000;
    int iEntry = 0;
    for (; my_tree->LoadTree(iEntry) >= 0; ++iEntry) {

        if (iEntry >= nMaxEntries)
            break;

        my_tree->GetEntry(iEntry);

        pedestal_int = static_cast<int>(pedestal*8 + 0.5);     // 3 bit shift and rounding

        ChannelInfo[channelId].push_back(pedestal_int);
        std::copy(std::begin(*ofc_a_int_ptr),std::end(*ofc_a_int_ptr),std::back_inserter(ChannelInfo[channelId]));
        std::copy(std::begin(*ofc_b_int_ptr),std::end(*ofc_b_int_ptr),std::back_inserter(ChannelInfo[channelId]));

        ChannelLocation[channelId].push_back(sc_eta);
        ChannelLocation[channelId].push_back(sc_phi);
        ChannelLocation[channelId].push_back(sc_det);
        ChannelLocation[channelId].push_back(sc_layer);

        if (doPrint) {

            std::cout << std::endl << "channelId= " << channelId << "  pedestal=" << pedestal << "  intPedestal=" << pedestal_int << std::endl;
   
            std::cout <<"Filter a:" << std::endl;
            std::copy(std::begin(*ofc_a_int_ptr),std::end(*ofc_a_int_ptr),std::ostream_iterator<int>(std::cout,",")); 
            std::cout << std::endl;
            std::copy(std::begin(*ofc_a_ptr),std::end(*ofc_a_ptr),std::ostream_iterator<float>(std::cout,",")); 
            std::cout << std::endl;
    
            std::cout <<"Filter b:" << std::endl;
            std::copy(std::begin(*ofc_b_int_ptr),std::end(*ofc_b_int_ptr),std::ostream_iterator<int>(std::cout,",")); 
            std::cout << std::endl;
            std::copy(std::begin(*ofc_b_ptr),std::end(*ofc_b_ptr),std::ostream_iterator<float>(std::cout,",")); 
            std::cout << std::endl;

            std::cout << "SC eta= " << sc_eta << "  phi=" << sc_phi << "  layer=" << sc_layer << std::endl;

        }

    }    

   std::cout << "Done, loaded " << iEntry << " entries" << std::endl;

}


void Analysis(){

    std::cout << "In Analysis() function" << std::endl;

    TFile * File = new TFile("test1.root","recreate");

    gStyle->SetOptStat(0);

    ReadChannelInfoFromRoot("filter/test_db_run438638_DBtest.root");

    TString path="";
    TString path1="/disk/moose/atlas/Panagiotis/";
    // vector<TString> c{path1+"calib_442993_LArNoise.root"};
    vector<TString> c{path1+"test.root"};

    myfile1.open ("noise.txt");

    R.push_back(new Run("_","Run",c,1));

    for (int i=0; i<R.size(); i++){R[i]->Analyze();}
    // for (int i=0; i<R.size(); i++){R[i]->Analyze_2Dversion();}
     myfile1.close();


///--------------- Drawing ------------------------------------------------


vector <TH1*> H;

for (int j=0; j<1; j++) {
   for (int i=0; i<R[0]->GetHistos(Categories[j]).size(); i++){
          for (int k=0; k<R.size(); k++){
            H.push_back(R[k]->GetHistos(Categories[j])[i]); }
          DrawMultiHistos(H,"Run",0);
          H.clear();}

    File->Close();
}

///----------------------------------------------------------------------

}


void DrawMultiHistos(vector<TH1*> h, TString LegTitle, bool Nor){

    std::cout << "In DrawMultiHistos() function" << std::endl;

    TCanvas* C=new TCanvas();
    TPaveText* label = new TPaveText(0.18, 0.75, 0.49, 0.93, "ndc");
    label->SetBorderSize(0);
    label->SetFillStyle(0);
    label->SetTextAlign(13);

    TText* text = label->AddText("#it{ATLAS} #bf{Work in Progress}");
    text->SetTextSize(0.04);
    // text = label->AddText("#bf{Run 00440407}");
	// text->SetTextSize(0.045);

    TLegend* leg1 = new TLegend(0.63,0.65,0.88,0.85, "", "brNDC");
    //leg1->SetFillStyle(0);
    // leg1->SetLineColor(0);
    // leg1->SetHeader(LegTitle);

    int color;

    for(int i=0; i<h.size(); i++){

        color=i+1;
        if(i==0) color=4;
        if(i==4) color=401;
        if(i==9) color=16;

        TString *title= new TString(h[i]->GetTitle());
        TString *name = new TString(h[i]->GetName());
        h[i]->Draw("hist same");
        h[i]->SetLineColor(color);
        h[i]->SetMarkerColor(color);
        h[i]->SetTitle("");
        h[i]->SetLineWidth(2);
        // leg1->AddEntry(h[i],title->Data());

        if(Nor==true) h[i]->Scale(1./(h[i]->Integral()));

        if (name->Contains("log"))         {C->SetLogy();
                                           h[i]->GetYaxis()->SetRangeUser(1,100*h[i]->GetBinContent(h[i]->GetMaximumBin()));}
        else                               h[i]->GetYaxis()->SetRangeUser(0,1.5*h[i]->GetBinContent(h[i]->GetMaximumBin()));

        h[i]->GetYaxis()->SetTitleOffset(1.2);
    }

    label->Draw();
    // leg1->Draw();

    C->Write(h[0]->GetName());

    string Save(h[0]->GetName());
    TString SaveAs="Histos/"+Save+".png";

    C->SaveAs(SaveAs);

}
 
int main(int argc, char **argv)
{  
    std::cout << "Starting main()" << std::endl;


    std::cout << sizeof(int) << std::endl;
    std::cout << sizeof(long) << std::endl;



    Analysis();                 // run the analysis

    return(0);
}

